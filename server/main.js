import { Meteor } from 'meteor/meteor';
import { ProductsCollection } from '../imports/api/products';
import { LinksCollection } from '/imports/api/links';

function insertLink({ title, url }) {
    LinksCollection.insert({ title, url, createdAt: new Date() });
}

function insertProduct({ name, stock }) {
    ProductsCollection.insert({ name, stock, createdAt: new Date() });
}

Meteor.startup(() => {
    // If the Links collection is empty, add some data.
    if (LinksCollection.find().count() === 0) {
        insertLink({
            title: 'Do the Tutorial',
            url: 'https://www.meteor.com/tutorials/react/creating-an-app'
        });

        insertLink({
            title: 'Follow the Guide',
            url: 'http://guide.meteor.com'
        });

        insertLink({
            title: 'Read the Docs',
            url: 'https://docs.meteor.com'
        });

        insertLink({
            title: 'Discussions',
            url: 'https://forums.meteor.com'
        });
    }

    if (ProductsCollection.find().count() === 0) {
        insertProduct({
            name: 'Do the Tutorial',
            stock: 10
        });

        insertProduct({
            name: 'Follow the Guide',
            stock: 10
        });

        insertProduct({
            name: 'Read the Docs',
            stock: 10
        });

        insertProduct({
            name: 'Discussions',
            stock: 10
        });
    }
});