import React, { useState } from 'react';
import { ProductsCollection } from '../api/products';

export const ProductForm = () => {
  const [name, setName] = useState("");
  const [stock, setStock] = useState("");

  const handleSubmit = e => {
    e.preventDefault();

    if (!name || !stock) return;

    ProductsCollection.insert({
      name: name.trim(),
      stock: stock,
      createdAt: new Date()
    });

    setName("");
    setStock("");
  };

  return (
    <form className="task-form" onSubmit={handleSubmit}>
        <input
            type="text"
            value={name}
            placeholder="Product Name"
            onChange={(e) => setName(e.target.value)}
        />
        <br />
        <input
            type="number"
            value={stock}
            placeholder="Quantity"
            onChange={(e) => setStock(e.target.value)}
        />
        <br />
        <button type="submit">Save Product</button>
    </form>
  );
};