import React from 'react';
import { ProductsCollection } from '../api/products.js';
import { Hello } from './Hello.jsx';
import { Info } from './Info.jsx';
import { Product } from './Product.jsx';
import { ProductForm } from './ProductForm.jsx';

export const App = () => (
  <div>
    <h1>Welcome to Meteor!</h1>
    <Hello/>
    <Info/>
    <Product/>

    <ProductForm/>
  </div>
);
