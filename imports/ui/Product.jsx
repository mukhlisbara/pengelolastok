import React from 'react';
import { useTracker } from 'meteor/react-meteor-data';
import { ProductsCollection } from '../api/products';

const deleteProduct = ({ _id }) => ProductsCollection.remove(_id);

const editProduct = ({ _id }) => ProductsCollection.update(_id, {
  $set: {
    stock: 100
  }
});

export const Product = () => {
  const products = useTracker(() => {
    return ProductsCollection.find().fetch();
  });

  return (
    <div>
      <h2>Learn Meteor, ver. 2!</h2>
      <table>
        <thead>
          <tr>
            <th>Nama</th>
            <th>Stok</th>
            <th>Tanggal</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          {products.map(
            item => <tr key={item._id}>
              <td>{item.name}</td>
              <td>{item.stock}</td>
              <td>{item.createdAt.getUTCDate()}-{item.createdAt.getUTCMonth()+1}-{item.createdAt.getUTCFullYear()}</td>
              <td>
                <button onClick={ () => deleteProduct(item) }>Hapus</button>
                <button onClick={ () => editProduct(item) }>Edit</button>
              </td>
            </tr>
          )}
        </tbody>
      </table>
    </div>
  );
};
